sap.ui.define([
	"sap/ui/core/Control",
	"sap/ui/model/json/JSONModel"
], function(Control, JSONmodel) {
	"use strict";

	return Control.extend("com.teampro.utils.ClearValidation", {
		constructor: function(oIndicatorButton, aClassArray) {

			this._sLabelClass = "sap.m.Label";
			this._aClassesToBeValidated = ["sap.m.ComboBox", "sap.m.MultiInput", "sap.m.Select", "sap.m.Input", "sap.m.DatePicker",
				"sap.m.TimePicker",
				"sap.m.MultiComboBox", "sap.m.TextArea"
			];
			if (aClassArray) {
				this._aClassesToBeValidated = aClassArray;
			}
	
			this.oIndicatorButton = oIndicatorButton;
	

		},

		validateElements: function(oElement) {
			var elementsToValidate = oElement.findElements();
			for (var i = 0; i < elementsToValidate.length; i++) {
				this.validateElements(elementsToValidate[i]);
			}

			var className = oElement.getMetadata().getName();
			if (this._aClassesToBeValidated.indexOf(className) > -1) {
				oElement.setValueState(sap.ui.core.ValueState.None);
				return true;
			}
		}
	});
});