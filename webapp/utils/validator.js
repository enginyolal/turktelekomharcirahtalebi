sap.ui.define([
	"sap/ui/core/Control",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessagePopover",
	"sap/m/MessagePopoverItem"
], function(Control, JSONmodel, MessagePopover, MessagePopoverItem) {
	"use strict";

	return Control.extend("com.teampro.utils.validator", {
		constructor: function(oIndicatorButton, aClassArray) {
			if (oIndicatorButton) {
				this._oIndicatorButton = oIndicatorButton;
				this._oIndicatorButton.attachPress(this.onMessageIndicatorButtonPress.bind(this));
			}

			this._sLabelClass = "sap.m.Label";
			this._aClassesToBeValidated = ["sap.m.ComboBox", "sap.m.MultiInput", "sap.m.Select", "sap.m.Input", "sap.m.DatePicker",
				"sap.m.MultiComboBox", "sap.m.TextArea", "sap.m.TimePicker"
			];
			if (aClassArray) {
				this._aClassesToBeValidated = aClassArray;
			}

			this._oValidatorMessengerModel = new JSONmodel();
			this._oValidatorMessengerModel.setData([]);

			this.prepareProjectSpecificValidations();
		},

		/* validateElements
		 * oElement: element to recursively crawl deeper and validate
		 * bMessage: true:to show to error messages on view, false:not
		 */
		validateElements: function(oElement, bMessage) {
			if (!this._oBundle) {
				this._oBundle = oElement.getModel("i18n").getResourceBundle();
			}

			var isValid = true;
			var isValidProjectSpecific = true;

			// Validate deeper elements first
			var elementsToValidate = oElement.findElements();
			for (var i = 0; i < elementsToValidate.length; i++) {
				if (!this.validateElements(elementsToValidate[i], bMessage)) {
					isValid = false; // Herhangi bir child element false döndükten sonra tekrar true"ya dönmesin diye if"in içine yazıldı
				}
			}

			// Check if the element has a type that we want to validate
			var className = oElement.getMetadata().getName();
			if (this._aClassesToBeValidated.indexOf(className) > -1) {
				// Delete previous error messages for clean validation
				if (bMessage) {
					this.deleteErrorMessageFromArray(oElement);
				}

				// Validate For Project Specific Rules
				var controlId = this.getIdOfControlElement(oElement);
				if (this._aProjectSpecificValidations[controlId]) {
					isValidProjectSpecific = this._aProjectSpecificValidations[controlId](oElement, bMessage);
					if (isValid) {
						isValid = isValidProjectSpecific;
					}
				}

				// Validate For Required
				var required = this.isRequired(oElement);
				var value;
				var errorText;
				// required, visible, enabled and editable
				if (required && oElement.getVisible() && oElement.getEnabled()) {
					if (className !== "sap.m.Select") {
						if (!oElement.getEditable()) {
							return true;
						}
					}
					switch (className) {
						case "sap.m.DatePicker":
							value = oElement.getDateValue();
							errorText = "errorDateSelection";
							break;
						case "sap.m.TimePicker":
							value = oElement.getDateValue();
							errorText = "errorTimeSelection";
							break;
						case "sap.m.ComboBox":
							value = oElement.getSelectedKey();
							errorText = "errorSelection";
							break;
						case "sap.m.MultiComboBox":
							value = oElement.getSelectedKeys().length;
							errorText = "errorMultipleSelection";
							break;
						case "sap.m.Select":
							value = oElement.getSelectedKey();
							errorText = "errorSelection";
							break;
						case "sap.m.MultiInput":
							value = oElement.getTokens().length;
							errorText = "errorValue";
							break;

						default:
							value = oElement.getValue();
							errorText = "errorValue";
					}
					if (!value) {
						isValid = false;
						this.addError(oElement, errorText, bMessage);
					} else if (oElement.getValueState() === "Error" && isValidProjectSpecific) {
						oElement.setValueState(sap.ui.core.ValueState.None);
					}
				}
			}
			return isValid;
		},

		validateTable: function(oElement, sTableName) {
			var messagesArray = this._oValidatorMessengerModel.getData();
			var sError = "errorProdTable";

			// Delete previous messages
			for (var i = 0; i < messagesArray.length; i++) {
				if (messagesArray[i].title === this._oBundle.getText(sError) &&
					messagesArray[i].subtitle === sTableName) {
					messagesArray.splice(i, 1);
					break;
				}
			}

			if (!oElement.getModel("prodScope").getProperty("/products") || oElement.getModel("prodScope").getProperty("/products").length ===
				0) {
				messagesArray.push({
					type: sap.ui.core.ValueState.Error,
					title: this._oBundle.getText(sError),
					description: this._oBundle.getText(sError),
					subtitle: sTableName
				});
				//Set data
				this._oValidatorMessengerModel.setData(messagesArray);
				this._oIndicatorButton.setVisible(true);
				this._oIndicatorButton.setText(messagesArray.length);
				return false;
			}
			//If valid delete error
			this._oValidatorMessengerModel.setData(messagesArray);
			this._oIndicatorButton.setText(messagesArray.length);
			if (messagesArray.length === 0) {
				this._oIndicatorButton.setVisible(false);
			}

			return true;
		},

		getLabel: function(oElement) {
			var parent = oElement.getParent();
			var arrayOfElements = parent.findElements();
			for (var i = 0; i < arrayOfElements.length; i++) {
				if (arrayOfElements[i].getMetadata().getName() === this._sLabelClass) {
					if (arrayOfElements[i].__sLabeledControl === oElement.getId()) {
						return arrayOfElements[i].getText();
					}
				}
			}
		},

		getIdOfControlElement: function(oElement) {
			var id = oElement.getId();
			var result = id.split("--");
			return result[result.length - 1];
		},

		addError: function(oElement, sError, bMessage) {
			oElement.setValueState(sap.ui.core.ValueState.Error);
			oElement.setValueStateText(this._oBundle.getText(sError));
			if (bMessage) {
				var aMessages = this._oValidatorMessengerModel.getData();
				aMessages.push({
					type: sap.ui.core.ValueState.Error,
					title: this._oBundle.getText(sError),
					description: this._oBundle.getText(sError),
					subtitle: this.getLabel(oElement)
				});
				//Set data
				this._oValidatorMessengerModel.setData(aMessages);
				this._oIndicatorButton.setText(aMessages.length);
				this._oIndicatorButton.setVisible(true);
			}
		},

		deleteErrorMessageFromArray: function(oElement) {
			var aMessages = this._oValidatorMessengerModel.getData();
			for (var i = 0; i < aMessages.length; i++) {
				if (aMessages[i].title === oElement.getValueStateText() &&
					aMessages[i].subtitle === this.getLabel(oElement)) {
					aMessages.splice(i, 1);
					break;
				}
			}
			this._oValidatorMessengerModel.setData(aMessages);
			if (aMessages.length === 0) {
				this._oIndicatorButton.setVisible(false);
			}
		},

		isRequired: function(oElement) {
			// Get label of the input element
			var parent = oElement.getParent();
			var arrayOfElements = parent.findElements();
			for (var i = 0; i < arrayOfElements.length; i++) {
				if (arrayOfElements[i].getMetadata().getName() === this._sLabelClass) {
					if (arrayOfElements[i].__sLabeledControl === oElement.getId()) {
						return arrayOfElements[i].getRequired();
					}
				}
			}
		},

		onMessageIndicatorButtonPress: function(oEvent) {
			if (!this._oMessagePopover) {
				this._oMessagePopover = new MessagePopover({
					items: {
						path: "messageList>/",
						template: new MessagePopoverItem({
							type: "{messageList>type}",
							title: "{messageList>title}",
							description: "{messageList>description}",
							subtitle: "{messageList>subtitle}",
							counter: "{messageList>counter}"
						})
					}
				});
				this._oMessagePopover.setModel(this._oValidatorMessengerModel, "messageList");
			}

			if (this._oMessagePopover.isOpen()) {
				this._oMessagePopover.close();
			} else {
				this._oMessagePopover.openBy(oEvent.getSource());
			}
		},

		prepareProjectSpecificValidations: function() {
			// Each function has to take two arguments
			// 1.oElement (control element)
			this._aProjectSpecificValidations = [];
			this._aProjectSpecificValidations.approvalDate = function(oElement, bMessage) { // Dikkat
				if (!oElement.getDateValue()) {
					return true; //actually it is not valid, but it will throw required error 1st
				}
				var expDate = new Date(oElement.getModel("dialogModel").getProperty("/OM_GENEL/HEADER/GECBAS"));
				var appDate = oElement.getDateValue().getTime();
				if (expDate > appDate) {
					this.addError(oElement, "errorApprovalnDate", bMessage);
					return false;
				}
				return true;
			}.bind(this);
		}
	});
});