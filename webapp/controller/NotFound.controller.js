sap.ui.define([
	"com/assistt/harcirahtalep/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("com.assistt.harcirahtalep.controller.NotFound", {

		/**
		 * Navigates to the worklist when the link is pressed
		 * @public
		 */
		onLinkPressed: function() {
			this.getRouter().navTo("worklist");
		}

	});

});