sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox",
	"sap/m/MessageToast"
], function(Controller, History, MessageBox, MessageToast) {
	"use strict";

	return Controller.extend("com.assistt.harcirahtalep.controller.BaseController", {

		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		getModel: function(sName) {
			return this.getView().getModel(sName);
		},

		setModel: function(oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		getResourceBundle: function() {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		getMessage: function(msgName) {
			var message = this.getOwnerComponent().getModel("i18n").getResourceBundle().getText(msgName);
			MessageToast.show(message, {
				width: "25em"
			});
		},

		onShareEmailPress: function() {
			var oViewModel = (this.getModel("objectView") || this.getModel("worklistView"));
			sap.m.URLHelper.triggerEmail(
				null,
				oViewModel.getProperty("/shareSendEmailSubject"),
				oViewModel.getProperty("/shareSendEmailMessage")
			);
		},

		onNavBack: function() {
			var sPreviousHash = History.getInstance().getPreviousHash(),
				oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");

			if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
				history.go(-1);
			} else {
				this.getRouter().navTo("worklist", {}, true);
			}
		},
		onDialogCancel: function(oEvent) {
			oEvent.getSource().getParent().close();
		},

		getClearedData: function(aExpandedSet, oData) {
			
			for (var i1 in aExpandedSet) {
				var sPath1 = aExpandedSet[i1].path;
				oData[sPath1] = oData[sPath1].results;

				for (var j1 in oData[sPath1]) {
					delete oData[sPath1][j1].__metadata;
					if (aExpandedSet[i1].subPaths) {
						oData[sPath1][j1] = this.getClearedData(aExpandedSet[i1].subPaths, oData[sPath1][j1]);
					}
				}
			}
			delete oData.__metadata;
			return oData;
		}

	});

});