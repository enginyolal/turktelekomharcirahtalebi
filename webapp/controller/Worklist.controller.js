sap.ui.define([
	"com/assistt/harcirahtalep/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/routing/History",
	"com/assistt/harcirahtalep/model/formatter",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"com/assistt/harcirahtalep/utils/Validator",
	"com/assistt/harcirahtalep/utils/ClearValidation"
], function(BaseController, JSONModel, History, formatter, Filter, FilterOperator, Validator, ClearValidation) {
	"use strict";

	return BaseController.extend("com.assistt.harcirahtalep.controller.Worklist", {

		formatter: formatter,

		onInit: function() {
			var iOriginalBusyDelay,
				oViewModel;

			this.getRouter().getRoute("worklist").attachPatternMatched(this._onWorklistMatched, this);

			oViewModel = new JSONModel({
				busy: true,
				delay: 0,
				tripSelected: false
			});
			this.setModel(oViewModel, "worklistView");
			this.setModel(this._createNewModel(), "tripModel");
			this.setModel(this._createExpenseModel(), "expenseModel");
			this.setModel(this._createTransportationModel(), "transportationModel");
			this.setModel(this._createAccommodationModel(), "accommodationModel");
			this.setModel(this._createApproversModel(), "approversModel");

			iOriginalBusyDelay = this.getView().getBusyIndicatorDelay();
			this.setModel(oViewModel, "objectView");

			this.getOwnerComponent().getModel().metadataLoaded().then(function() {
				oViewModel.setProperty("/delay", iOriginalBusyDelay);
				oViewModel.setProperty("/busy", true);
			});
		},

		_onWorklistMatched: function(oEvent) {
			var oModel = this.getModel(),
				oViewModel = this.getModel("worklistView");

			oModel.metadataLoaded().then(function() {
				oViewModel.setProperty("/busy", false);
			}.bind(this));
		},

		onTripSelected: function(oEvent) {
			var oTripModel = this.getModel("tripModel"),
				oViewModel = this.getModel("worklistView"),
				oSelectedItem = oEvent.getSource().getBindingContext().getObject();

			oTripModel.setData({});
			delete oSelectedItem.__metadata;
			delete oSelectedItem.TransportationSet;
			delete oSelectedItem.AccomodationSet;
			oTripModel.setData(oSelectedItem);
			oSelectedItem.Begda.setHours(0, 0, 0, 0);
			oSelectedItem.Endda.setHours(0, 0, 0, 0);
			oTripModel.setProperty("/SyhFixedBegda", oSelectedItem.Begda);
			oTripModel.setProperty("/SyhFixedEndda", oSelectedItem.Endda);
			oViewModel.setProperty("/tripSelected", true);
			this._callReadTrip(oSelectedItem.Tlpno);

		},

		onNewExpense: function(oEvent) {
			var oExpenseModel = this.getModel("expenseModel"),
				oResourceBundle = this.getResourceBundle(),
				oTripModel = this.getModel("tripModel");

			if (!this._newExpenseDialog) {
				this._newExpenseDialog = sap.ui.xmlfragment("idExpenseDialog",
					"com.assistt.harcirahtalep.fragment.Dialog.NewExpense", this);
				this._newExpenseDialog.setModel(this.getModel("i18n"), "i18n");
				this._newExpenseDialog.setModel(this.getModel());
				this.getView().addDependent(this._newExpenseDialog);
			}
			if (this.lastTalepno !== oTripModel.getProperty("/Tlpno")) {//Talep numarası değişti ise masraf türleri güncellensin
				sap.ui.core.Fragment.byId("idExpenseDialog", "shMasrafturu").getBinding("items").filter([new Filter("Talepno", FilterOperator.EQ,
					oTripModel.getProperty("/Tlpno"))]);
				this.lastTalepno = oTripModel.getProperty("/Tlpno");
			}
			oExpenseModel.setProperty("/Entity", {});
			oExpenseModel.setProperty("/Entity/Action", oResourceBundle.getText("ekle"));
			this.getClearValidation().validateElements(sap.ui.core.Fragment.byId("idExpenseDialog", "sfNewExpense"));
			this.oldEditPath = "";
			this._newExpenseDialog.open();

		},

		onAddExpense: function(oEvent) {
			var oExpenseModel = this.getModel("expenseModel"),
				aExpenses = oExpenseModel.getProperty("/Expenses"),
				isValid;

			isValid = this.getValidator().validateElements(sap.ui.core.Fragment.byId("idExpenseDialog", "sfNewExpense"),
				false);
			if (!isValid) {
				return;
			}
			// Bu alana function import ile yeni masraf girişi kontrolü eklenecek  

			if (this.oldEditPath) {
				oExpenseModel.setProperty(this.oldEditPath, oExpenseModel.getProperty("/Entity"));
			} else {
				aExpenses.push(oExpenseModel.getProperty("/Entity"));
				oExpenseModel.setProperty("/Expenses", aExpenses);
			}
			this._newExpenseDialog.close();
		},

		onDeleteExpense: function(oEvent) {
			var oExpenseModel = this.getModel("expenseModel"),
				aTransportations = oExpenseModel.getProperty("/Expenses"),
				sPath = oEvent.getParameter("listItem").getBindingContext("expenseModel").getPath().split("/"),
				iIndex = parseInt(sPath[2], 10);

			aTransportations.splice(iIndex, 1);
			oExpenseModel.refresh();
		},

		onEditExpense: function(oEvent) {
			var oExpenseModel = this.getModel("expenseModel"),
				oEntity = jQuery.extend({}, oExpenseModel.getProperty(oEvent.getSource().getBindingContextPath()));

			this.oldEditPath = oEvent.getSource().getBindingContextPath();
			oExpenseModel.setProperty("/Entity", oEntity);
			oExpenseModel.setProperty("/Entity/Action", this.getResourceBundle().getText("degistir"));
			this._newExpenseDialog.open();
		},
		onComplete: function(oEvent) {
			var oExpenseModel = this.getModel("expenseModel"),
				aExpenses = oExpenseModel.getProperty("/Expenses");

			if (!aExpenses.length > 0) {
				sap.m.MessageToast.show(this.getResourceBundle().getText("masraf_girmelisiniz"));
				return;
			}
			var isValid1 = this.getValidator().validateElements(this.getView().byId(this.createId("sfTripHappening1")), true),
				isValid2 = this.getValidator().validateElements(this.getView().byId(this.createId("sfTripHappening2")), true);
			if (!isValid1 || !isValid2) {
				return;
			}

			this._callGetApprovers();

		},

		onSendApprove: function(oEvent) {
			this._callCreate();
		},

		_callCreate: function(sAction) {
			var oModel = this.getModel(),
				oViewModel = this.getModel("worklistView"),
				oTripModelData = this.getModel("tripModel").getData(),
				oExpenseModelData = this.getModel("expenseModel").getData(),
				oEntry = jQuery.extend({}, oTripModelData),
				aExpenses = jQuery.extend([], oExpenseModelData.Expenses);

			oViewModel.setProperty("/busy", true);

			oEntry.Masrafbilgileri = aExpenses;
			delete oEntry.SyhFixedBegda;
			delete oEntry.SyhFixedEndda;
			delete oEntry.Avans;
			delete oEntry.Atutar;

			aExpenses.forEach(function(Expense) {
				Expense.Begda.setHours(12);
				Expense.Tutar = Expense.Tutar.toString();
				delete Expense.Action;
				delete Expense.MasrafTuruText;

			});

			var oParams = {};
			oParams.success = function(oData, oResponse) {
				oViewModel.setProperty("/busy", false);
				sap.m.MessageToast.show(this.getResourceBundle().getText("harcirah_talebi_kaydedildi"));
				this._callClearModels();
				//		this._approversDialog.close();
			}.bind(this);

			oParams.error = function(oError) {
				oViewModel.setProperty("/busy", false);
			};
			oParams.async = true;
			oModel.create("/TripSet", oEntry, oParams);
		},

		_callGetApprovers: function() {
			var oApproversModel = this.getModel("approversModel"),
				oTripModelData = this.getModel("tripModel").getData(),
				oViewModel = this.getModel("worklistView"),
				sPath = "/ApproversSet",
				filterArr = [];

			filterArr.push(new sap.ui.model.Filter("Tlpno", sap.ui.model.FilterOperator.EQ, oTripModelData.Tlpno));
			filterArr.push(new sap.ui.model.Filter("Yurtdisi", sap.ui.model.FilterOperator.EQ, oTripModelData.Yurtdisi));

			var oParams = {};
			oParams.async = false;
			oParams.success = function(oData, oResponse) {
				oApproversModel.setProperty("/Approvers", oData.results);
				oViewModel.setProperty("/busy", false);
				this._openApproverDialog();
			}.bind(this);
			oParams.error = function() {
				oViewModel.setProperty("/busy", false);
			};
			oParams.filters = filterArr;
			oViewModel.setProperty("/busy", true);
			this.getView().getModel().read(sPath, oParams);
		},

		_openApproverDialog: function(oEvent) {
			if (!this._approversDialog) {
				this._approversDialog = sap.ui.xmlfragment("com.assistt.harcirahtalep.fragment.dialog.Approvers", this);
				this._approversDialog.setModel(this.getModel("i18n"), "i18n");
				this._approversDialog.setModel(this.getModel());
				this.getView().addDependent(this._approversDialog);
			}
			jQuery.sap.syncStyleClass(this.getOwnerComponent().getContentDensityClass(), this.getView(), this._approversDialog);
			this._approversDialog.open();
		},

		_callReadTrip: function(sTlpno) {
			var sObjectPath,
				mParameters,
				oClearedData,
				aExpandedSet,
				oModel = this.getModel(),
				oViewModel = this.getModel("objectView"),
				oTransportationModel = this.getModel("transportationModel"),
				oAccommodationModel = this.getModel("accommodationModel");

			sObjectPath = this.getModel().createKey("/TripSet", {
				Tlpno: sTlpno
			});
			mParameters = {
				urlParameters: {
					"$expand": "TransportationSet,AccomodationSet"
				},
				success: function(oData) {
					oViewModel.setProperty("/busy", false);
					aExpandedSet = [{
						path: "TransportationSet"
					}, {
						path: "AccomodationSet"
					}];
					oClearedData = this.getClearedData(aExpandedSet, oData);
					oTransportationModel.setProperty("/Transportations", oClearedData.TransportationSet);
					oAccommodationModel.setProperty("/Accommodations", oClearedData.AccomodationSet);
				}.bind(this),
				error: function(oError) {
					oViewModel.setProperty("/busy", false);
				}.bind(this)
			};

			oViewModel.setProperty("/busy", true);
			oModel.read(sObjectPath, mParameters);
		},

		_callClearModels: function() {
			var oTripModel = this.getModel("tripModel"),
				oExpenseModel = this.getModel("expenseModel"),
				oTransportationModel = this.getModel("transportationModel"),
				oAccommodationModel = this.getModel("accommodationModel"),
				oApproversModel = this.getModel("approversModel");

			oTripModel.setData({});
			oExpenseModel.setProperty("/Expenses", []);
			oExpenseModel.setProperty("/Entity", {});
			oTransportationModel.setProperty("/Transportations", []);
			oTransportationModel.setProperty("/Entity", {});
			oAccommodationModel.setProperty("/Accommodations", []);
			oAccommodationModel.setProperty("/Entity", {});
			oApproversModel.setProperty("/Approvers", []);
		},

		_createNewModel: function() {
			return new JSONModel({});
		},

		_createTransportationModel: function() {
			return new JSONModel({
				Entity: {},
				Transportations: []
			});
		},

		_createAccommodationModel: function() {
			return new JSONModel({
				Entity: {},
				Accommodations: []
			});
		},

		_createExpenseModel: function() {
			return new JSONModel({
				Entity: {},
				Expenses: []
			});
		},

		_createApproversModel: function() {
			return new JSONModel({
				Approvers: []
			});
		},

		getValidator: function() {
			if (!this.Validator) {
				this.Validator = new Validator(this.getView().byId(this.createId("idMessageIndicator")));
			}
			return this.Validator;
		},

		getClearValidation: function() {
			if (!this.ClearValidation) {
				this.ClearValidation = new ClearValidation();
			}
			return this.ClearValidation;
		},

		onNavBack: function() {
			var sPreviousHash = History.getInstance().getPreviousHash(),
				oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");

			if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
				history.go(-1);
			} else {
				oCrossAppNavigator.toExternal({
					target: {
						shellHash: "#Shell-home"
					}
				});
			}
		}
	});
});